const { EventListener } = require('./events')

let OwnerEvent = new EventListener();
OwnerEvent.contractEventListener("owner", "Admin", "autochannel",
    "KBA-Franchise", "BrandContract", "addBrandEvent");
