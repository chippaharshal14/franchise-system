const{ClientApplication}=require('./client')
let DealerClient=new ClientApplication()
const transientData={
    financialData:Buffer.from("xyz"),
    franchiseAgreements:Buffer.from("xyz"),
    customerData:Buffer.from("xyz"),
    supplychainInformation:Buffer.from("xyz")
}
DealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "autochannel",
    "KBA-Franchise",
    "RightsContract",
    "privateTxn",
    transientData,
    "registerRights",
    "R03",
    "B03",
    
    
).then(message => {
    console.log(message.toString())})