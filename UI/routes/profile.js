let profile={
    owner:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/owner.franchise.com",
        "CP":"../Network/vars/profiles/autochannel_connection_for_nodesdk.json"
    },
    dealer:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/dealer.franchise.com",
        "CP":"../Network/vars/profiles/autochannel_connection_for_nodesdk.json"        
    },
    outlet:{
        "Wallet":"../Network/vars/profiles/vscode/wallets/outlet.franchise.com",
        "CP":"../Network/vars/profiles/autochannel_connection_for_nodesdk.json"  
    }
}
module.exports={profile}