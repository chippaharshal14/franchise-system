var express = require('express');
var router = express.Router();
const { ClientApplication } = require('./client');
const { Events } = require('./events.js')
const eventClient = new Events();
eventClient.contractEventListner("owner", "Admin", "autochannel",
  "KBA-Franchise", "BrandContract", "addBrandEvent")

eventClient.blockEventListner("owner", "Admin", "autochannel", "KBA-Franchise");

/* GET home page. */
router.get("/", (req, res) => {
  res.send("Distributed Franchise System");
});

router.get('/event', function (req, res, next) {
  console.log("Event Response %%%$$^^$%%$", eventClient.getEvents().toString())
  var event = eventClient.getEvents().toString()
  res.send({ BrandEvent: JSON.parse(event) })
});

router.get('/dealer', function (req, res, next) {
  res.render('dealer', { title: 'Dealer Dashboard' });
});

// router.get('/event', function (req, res, next) {
//   console.log("Event Response %%%$$^^$%%$", eventClient.getEvents().toString())
//   var event = eventClient.getEvents().toString()
//   res.send({ carEvent: event })
//   // .then(array => {
//   //   console.log("Value is #####", array)
//   //   res.send(array);
//   // }).catch(err => {
//   //   console.log("errors are ", err)
//   //   res.send(err)
//   // })
//   // res.render('index', { title: 'Dealer Dashboard' });
// });


router.get('/getBrandDetails/:brandId', async function (req, res) {
  const brandId = req.params.brandId;

  let OutletClient = new ClientApplication();

  OutletClient.generateAndSubmitTxn(
      "outlet",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "BrandContract",
      "queryTxn",
      "",
      "getBrandDetails",
      brandId
  ).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
  }).catch((err) => {
      res.json({ message: `Error while fetching details of product ${brandId}` });
  });
});

router.get('/getBuyDetails/:buyId', async function (req, res) {
  const buyId = req.params.buyId;

  let OutletClient = new ClientApplication();

  OutletClient.generateAndSubmitTxn(
      "outlet",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "BuyContract",
      "queryTxn",
      "",
      "getBuyDetails",
      buyId
  ).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
  }).catch((err) => {
      res.json({ message: `Error while fetching details of product ${buyId}` });
  });
});


router.get('/getRightsDetails/:rightsId', async function (req, res) {
  const rightsId = req.params.rightsId;
  let DealerClient = new ClientApplication();
  DealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "autochannel",
    "KBA-Franchise",
    "RightsContract",
    "queryTxn",
    "",
    "getRightsDetails", rightsId).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
    }).catch(error => {
      //alert('Error occured')
      res.json({ message: `Error while fetching details of product ${rightsId}` });

    })

})

router.get('/applyToRights/:brandId', async function (req, res) {
  const brandId = req.params.brandId;
  let OutletClient = new ClientApplication();
  OutletClient.generateAndSubmitTxn(
    "outlet",
    "Admin",
    "autochannel",
    "KBA-Franchise",
    "RightsContract",
    "queryTxn",
    "",
    "applyToRights", brandId).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
    }).catch(error => {
      //alert('Error occured')
      res.json({ message: `Error while fetching details of product ${brandId}` });

    })

})

router.get('/requestToDealer/:brandId', async function (req, res) {
  const brandId = req.params.brandId;
  let OutletClient = new ClientApplication();
  OutletClient.generateAndSubmitTxn(
    "outlet",
    "Admin",
    "autochannel",
    "KBA-Franchise",
    "RightsContract",
    "queryTxn",
    "",
    "requestToDealer", brandId).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
    }).catch(error => {
      //alert('Error occured')
      res.json({ message: `Error while fetching details of product ${brandId}` });

    })

})

router.post('/register-Brand', async function (req, res) {
  const brandId = req.body.brandId;
  const outletNo = req.body.outletNo;
  const franchiseName = req.body.franchiseName;
  const BrandValue = req.body.BrandValue;
  const ownerName = req.body.ownerName
  let OwnerClient = new ClientApplication();

  OwnerClient.generateAndSubmitTxn(
      "owner",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "BrandContract",
      "invokeTxn",
      "",
      "registerBrand",
      brandId, outletNo, franchiseName, BrandValue, ownerName)
      .then(message => {
          res.status(200).send("Brand registered Successfully")
      }).catch(error => {
          res.status(500).send({ error: `Failed to register brand`, message: `${error}` })
      });
});

router.post('/register-Rights', async function (req, res) {
  const rightsId = req.body.rightsId;
  const brandId = req.body.brandId;
  const financialData = req.body.financialData;
  const franchiseAgreements = req.body.franchiseAgreements;
  const customerData = req.body.customerData;
  const supplychainInformation = req.body.supplychainInformation
  let DealerClient = new ClientApplication();

  const transientData = {
    financialData: Buffer.from(financialData),
    franchiseAgreements: Buffer.from(franchiseAgreements),
    customerData: Buffer.from(customerData),
    supplychainInformation: Buffer.from(supplychainInformation)
  }

  DealerClient.generateAndSubmitTxn(
      "dealer",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "RightsContract",
      "privateTxn",
      transientData,  
      "registerRights",
      rightsId, brandId)
      .then(message => {
          res.status(200).send("Rights added Successfully")
      }).catch(error => {
          res.status(500).send({ error: `Failed to register rights`, message: `${error}` })
      });
});

router.post('/buy-Franchise', async function (req, res) {
  const buyId = req.body.buyId;
  const brandId = req.body.brandId;
  const brandValue = req.body.brandValue;
  const assetType = req.body.assetType;
  const franchiseName = req.body.franchiseName;
  const outletNo = req.body.outletNo;
  const outletOwnerName = req.body.outletOwnerName
  let OutletClient = new ClientApplication();

  OutletClient.generateAndSubmitTxn(
      "outlet",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "BuyContract",
      "invokeTxn",
      "",
      "buyFranchise",
      buyId, brandId, brandValue, assetType, franchiseName, outletNo, outletOwnerName)
      .then(message => {
          res.status(200).send("Franchise bought Successfully")
      }).catch(error => {
          res.status(500).send({ error: `Failed to buy Franchise`, message: `${error}` })
      });
});

router.delete('/delete-Brand/:brandId', async function (req, res) {
  const brandId = req.params.brandId;
  let OwnerClient = new ClientApplication();

  OwnerClient.generateAndSubmitTxn(
      "owner",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "BrandContract",
      "invokeTxn",
      "",
      "deleteBrand",
       brandId)
      .then(message => {
        console.log("Message response", message);
    res.status(200).json({ message: `Successfully deleted buy with ID ${brandId}` });

      }).catch(error => {
          res.status(500).send({ error: `Failed to delete brand`, message: `${error}` })
      });
});


router.get('/queryAllBrands', function (req, res, next) {
  let OutletClient = new ClientApplication();
  OutletClient.generateAndSubmitTxn(
    "outlet",
    "Admin",
    "autochannel",
    "KBA-Franchise",
    "BrandContract",
    "queryTxn",
    "",
    "queryAllBrands"
  )
    .then(cars => {
      const dataBuffer = cars.toString();
      const value = JSON.parse(dataBuffer)
      res.json(value);
    }).catch(err => {
      res.render("error", {
        message: `Some error occured`,
        callingScreen: "error",
      })
    })
});

router.get('/queryAllRights', function (req, res, next) {
  let OutletClient = new ClientApplication();
  OutletClient.generateAndSubmitTxn(
    "outlet",
    "Admin",
    "autochannel",
    "KBA-Franchise",
    "RightsContract",
    "queryTxn",
    "",
    "queryAllRights"
  )
    .then(cars => {
      const dataBuffer = cars.toString();
      const value = JSON.parse(dataBuffer)
      res.json(value);
    }).catch(err => {
      res.render("error", {
        message: `Some error occured`,
        callingScreen: "error",
      })
    })
});

router.get('/getBrandHistory/:brandId', function (req, res, next) {
  const brandId = req.params.brandId;

  let DealerClient = new ClientApplication();

  DealerClient.generateAndSubmitTxn(
      "dealer",
      "Admin",
      "autochannel",
      "KBA-Franchise",
      "BrandContract",
      "queryTxn",
      "",
      "getBrandHistory",
      brandId
  ).then(message => {
      console.log("Message response", message)
      messgae = JSON.stringify(message);
      message = JSON.parse(message)
      res.json(message);
  }).catch((err) => {
      res.json({ message: `Error while fetching details of product ${brandId}` });
  });
});



router.get('/addBrandEvent', async function (req, res, next) {
  let OwnerClient = new ClientApplication();
  const result = await OwnerClient.contractEventListner("owner", "Admin", "autochannel",
    "KBA-Franchise", "addBrandEvent")
  console.log("The result is ####$$$$", result)
  res.render('owner', { view: "BrandEvents", results: result })
})






module.exports = router;








