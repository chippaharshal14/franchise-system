/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { ChaincodeStub, ClientIdentity } = require('fabric-shim');
const { BrandContract } = require('..');
const winston = require('winston');

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.should();
chai.use(chaiAsPromised);
chai.use(sinonChai);

class TestContext {

    constructor() {
        this.stub = sinon.createStubInstance(ChaincodeStub);
        this.clientIdentity = sinon.createStubInstance(ClientIdentity);
        this.logger = {
            getLogger: sinon.stub().returns(sinon.createStubInstance(winston.createLogger().constructor)),
            setLevel: sinon.stub(),
        };
    }

}

describe('BrandContract', () => {

    let contract;
    let ctx;

    beforeEach(() => {
        contract = new BrandContract();
        ctx = new TestContext();
        ctx.stub.getState.withArgs('1001').resolves(Buffer.from('{"value":"brand 1001 value"}'));
        ctx.stub.getState.withArgs('1002').resolves(Buffer.from('{"value":"brand 1002 value"}'));
    });

    describe('#brandExists', () => {

        it('should return true for a brand', async () => {
            await contract.brandExists(ctx, '1001').should.eventually.be.true;
        });

        it('should return false for a brand that does not exist', async () => {
            await contract.brandExists(ctx, '1003').should.eventually.be.false;
        });

    });

    describe('#createBrand', () => {

        it('should create a brand', async () => {
            await contract.createBrand(ctx, '1003', 'brand 1003 value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1003', Buffer.from('{"value":"brand 1003 value"}'));
        });

        it('should throw an error for a brand that already exists', async () => {
            await contract.createBrand(ctx, '1001', 'myvalue').should.be.rejectedWith(/The brand 1001 already exists/);
        });

    });

    describe('#readBrand', () => {

        it('should return a brand', async () => {
            await contract.readBrand(ctx, '1001').should.eventually.deep.equal({ value: 'brand 1001 value' });
        });

        it('should throw an error for a brand that does not exist', async () => {
            await contract.readBrand(ctx, '1003').should.be.rejectedWith(/The brand 1003 does not exist/);
        });

    });

    describe('#updateBrand', () => {

        it('should update a brand', async () => {
            await contract.updateBrand(ctx, '1001', 'brand 1001 new value');
            ctx.stub.putState.should.have.been.calledOnceWithExactly('1001', Buffer.from('{"value":"brand 1001 new value"}'));
        });

        it('should throw an error for a brand that does not exist', async () => {
            await contract.updateBrand(ctx, '1003', 'brand 1003 new value').should.be.rejectedWith(/The brand 1003 does not exist/);
        });

    });

    describe('#deleteBrand', () => {

        it('should delete a brand', async () => {
            await contract.deleteBrand(ctx, '1001');
            ctx.stub.deleteState.should.have.been.calledOnceWithExactly('1001');
        });

        it('should throw an error for a brand that does not exist', async () => {
            await contract.deleteBrand(ctx, '1003').should.be.rejectedWith(/The brand 1003 does not exist/);
        });

    });

});
