/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const BrandContract = require('./lib/brand-contract');
const RightsContract = require('./lib/rights-contract');
const BuyContract = require('./lib/buy-contract');

module.exports.BrandContract = BrandContract;
module.exports.RightsContract = RightsContract;
module.exports.BuyContract = BuyContract;
module.exports.contracts = [ BrandContract, RightsContract, BuyContract ];
