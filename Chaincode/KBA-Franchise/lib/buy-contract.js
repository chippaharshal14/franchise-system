/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');
const BrandContract = require('./brand-contract');

class BuyContract extends Contract {

    async buyExists(ctx, buyId) {
        const buffer = await ctx.stub.getState(buyId);
        return (!!buffer && buffer.length > 0);
    }

    async buyFranchise(ctx, buyId, brandId, brandValue, assetType, franchiseName, outletNo, outletOwnerName) {
        const brandContract=new BrandContract();
        const mspId = ctx.clientIdentity.getMSPID();

        if (mspId === "outlet-franchise-com") {
            const exists = await this.buyExists(ctx, buyId);
            const exists2 = await brandContract.brandExists(ctx, brandId)
            if (exists) {
                throw new Error(`The asset ${buyId} already sold`);
            }
            if (!exists2) {
                throw new Error(`The brand ${brandId} not registered`);
            }

            const asset = {
                brandValue,
                assetType,
                franchiseName,
                outletNo,
                outletOwnerName,
            };

            const buffer = Buffer.from(JSON.stringify(asset));
            let addBuyEventData = { Type: 'Buy creation', buyId};
            await ctx.stub.setEvent('addBuyEvent', Buffer.from(JSON.stringify(addBuyEventData)));
            await ctx.stub.putState(buyId, buffer);

            return `Franchise outlet no.${outletNo} is sold to ${outletOwnerName} successfully`

        } else {
            return `User under the following MSP: ${mspId} cannot perform this action`
        }
    }

    async getBuyDetails(ctx, buyId) {
        const exists = await this.buyExists(ctx, buyId);
        if (!exists) {
            throw new Error(`The buy ${buyId} does not exist`);
        }
        const buffer = await ctx.stub.getState(buyId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }

}

module.exports = BuyContract;
