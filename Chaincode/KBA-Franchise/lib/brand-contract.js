/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

class BrandContract extends Contract {

    async brandExists(ctx, brandId) {
        const buffer = await ctx.stub.getState(brandId);
        return (!!buffer && buffer.length > 0);
    }

    async registerBrand(ctx, brandId, outletNo, franchiseName, BrandValue, ownerName) {
        const mspId = ctx.clientIdentity.getMSPID();

        if (mspId === "owner-franchise-com") {
            const exists = await this.brandExists(ctx, brandId);
            if (exists) {
                throw new Error(`The car ${brandId} already exists`);
            }

            const asset = {
                outletNo,
                franchiseName,
                BrandValue,
                status: 'Permission Granted',
                ownedBy: ownerName,
                assetType: 'brand'
            };

            const buffer = Buffer.from(JSON.stringify(asset));
            let addBrandEventData = { Type: 'Brand creation', FranchiseName: franchiseName };
            await ctx.stub.setEvent('addBrandEvent', Buffer.from(JSON.stringify(addBrandEventData)));
            await ctx.stub.putState(brandId, buffer);

            return `${franchiseName} Brand registered successfully`

        } else {
            return `User under the following MSP: ${mspId} cannot perform this action`
        }

    }

    
    async getBrandDetails(ctx, brandId) {
        const exists = await this.brandExists(ctx, brandId);
        if (!exists) {
            throw new Error(`The brand ${brandId} does not exist`);
        }
        const buffer = await ctx.stub.getState(brandId);
        const asset = JSON.parse(buffer.toString());
        return asset;
    }


    async deleteBrand(ctx, brandId) {
        const exists = await this.brandExists(ctx, brandId);
        if (!exists) {
            throw new Error(`The brand ${brandId} does not exist`);
        }
        await ctx.stub.deleteState(brandId);
    }

    async queryAllBrands(ctx) {

        const queryString = {
            selector: {
                assetType: 'brand'
            }
        }
        let resultIterator = await ctx.stub.getQueryResult(JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator, false)
        return JSON.stringify(result)

    }

    async getAllResults(iterator, isHistory) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                if (isHistory && isHistory == true) {
                    jsonRes.TxId = res.value.txId
                    jsonRes.TimeStamp = res.value.timestamp
                    jsonRes.Record = JSON.parse(res.value.value.toString())

                }
                else {
                    jsonRes.Key = res.value.key
                    jsonRes.Record = JSON.parse(res.value.value.toString())
                }
                allResult.push(jsonRes)

            }
        }
        await iterator.close()
        return allResult
    }

    async getBrandHistory(ctx, brandId) {
        let resultIterator = await ctx.stub.getHistoryForKey(brandId)
        let result = await this.getAllResults(resultIterator, true)
        return JSON.stringify(result)
    }



}

module.exports = BrandContract;
