/*
 * SPDX-License-Identifier: Apache-2.0
 */

'use strict';

const { Contract } = require('fabric-contract-api');

async function getCollectionName(ctx) {
    const collectionName = `CollectionRights`;
    return collectionName;
}

class RightsContract extends Contract {

    async rightsExists(ctx, rightsId) {
        const collectionName = await getCollectionName(ctx);
        const data = await ctx.stub.getPrivateDataHash(collectionName, rightsId);
        return (!!data && data.length > 0);
    }
    async applyToRights(ctx, brandId) {
        const exists5 = await this.brandExists1(ctx,brandId)
        if (!exists5) {
            return JSON.stringify("Rights does not exist, your application will not be forwarded");
        }
        return JSON.stringify("Rights exist, your application to buy rights is further done by dealer");
    }

    async requestToDealer(ctx, brandId) {
        const exists6 = await this.brandExists1(ctx,brandId)
        if (!exists6) {
            return JSON.stringify("Brand does not exist, so request not accepted");
        }
        return JSON.stringify("Request accepted");
    }

    async brandExists1(ctx, brandId) {
        const buffer = await ctx.stub.getState(brandId);
        return (!!buffer && buffer.length > 0);
    }

    async registerRights(ctx, rightsId, brandId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'dealer-franchise-com') {
            const exists1 = await this.rightsExists(ctx,rightsId);
            const exists = await this.brandExists1(ctx, brandId);
            if(exists1){
                throw new Error(`The rights asset for ${rightsId} already exists`);
            }
            if (!exists) {
                throw new Error(`The brand asset for ${brandId} does not exists`);
            }

            const rightsAsset = {};

            const transientData = ctx.stub.getTransient();
            if (transientData.size === 0 || !transientData.has('financialData') || !transientData.has('franchiseAgreements') || !transientData.has('customerData') || !transientData.has('supplychainInformation')) {
                throw new Error('The privateValue key was not specified in transient data. Please try again.');
            }
            rightsAsset.financialData = transientData.get('financialData').toString();
            rightsAsset.franchiseAgreements = transientData.get('franchiseAgreements').toString();
            rightsAsset.customerData = transientData.get('customerData').toString();
            rightsAsset.supplychainInformation = transientData.get('supplychainInformation').toString();
            rightsAsset.assetType = 'rights';

            const collectionName = await getCollectionName(ctx);
            await ctx.stub.putPrivateData(collectionName, rightsId, Buffer.from(JSON.stringify(rightsAsset)));
        } else {
            return `User under the following MSP: ${mspID} cannot perform this action`
        }
        return "Rights to buy franchise is done successfully"
    }

    async queryAllRights(ctx) {
        const queryString = {
            selector: { assetType: 'rights' }
        }
        const collectionName = await getCollectionName(ctx);
        let resultIterator = await ctx.stub.getPrivateDataQueryResult(collectionName, JSON.stringify(queryString))
        let result = await this.getAllResults(resultIterator.iterator)
        return JSON.stringify(result)
    }

    async getAllResults(iterator) {
        let allResult = []
        for (let res = await iterator.next(); !res.done; res = await iterator.next()) {
            if (res.value && res.value.value.toString()) {
                let jsonRes = {}
                jsonRes.Key = res.value.Key
                jsonRes.Record = JSON.parse(res.value.value.toString())
                allResult.push(jsonRes)
            }
        }
        await iterator.close()
        return allResult
    }

    async getRightsDetails(ctx, rightsId) {
        const exists = await this.rightsExists(ctx, rightsId);
        if (!exists) {
            throw new Error(`The asset rights ${rightsId} does not exist`);
        }
        let privateDataString;
        const collectionName = await getCollectionName(ctx);
        const privateData = await ctx.stub.getPrivateData(collectionName, rightsId);
        privateDataString = JSON.parse(privateData.toString());
        return privateDataString;
    }

    async deleteRights(ctx, rightsId) {
        const mspID = ctx.clientIdentity.getMSPID();
        if (mspID === 'owner-franchise-com') {
            const exists = await this.rightsExists(ctx, rightsId);
            if (!exists) {
                throw new Error(`The asset rights ${rightsId} does not exist`);
            }
            const collectionName = await getCollectionName(ctx);
            await ctx.stub.deletePrivateData(collectionName, rightsId);
        } else {
            return `User under the following MSP: ${mspID} cannot perform this action`
        }
    }
    

}

module.exports = RightsContract;
