spec.yaml contains the organisations details and peers for that organisations.

startNetwork.sh contains the code to start the network , creation of channel and
creation of genesis block.

Terminal

After cloning respective code, open the Network folder in terminal and run the
startNetwork.sh, before running check whether the read permission is available or
not,if not then run chmod +x startNetwork.sh
before running the startNetwork.sh file check whether any container is running or
not using docker ps -a
If the container is running then use docker container prune command
Then use docker volume ls to check the volume mapping, if present then use docker
volume prune.

Deploying Chaincode:

-Add the respective wallets,and gateways add the smart contract from IBM extension
default contract and give the name of the contract .
-To add the smart contract, Package it with tar file.
-If you do any changes, then update the version in package.json file and again package
the chaincode.
-To add the Private data contract ,select Private Data Contract option, then
automatically collections.json file will be created there mentioning the policy among
which organisation the data should be shared.
-After packaging the chaincode, we need to deploy the chaincode in environment
-Click on ecommchannel in Fabric Environment, under that click on deploy smart
contract.
-Deploy smart Contract window will appear, add the chaincode which was packaged
lastly and give next then add the collections.json and click on deploy button.
-Perform the operation by selecting the respective gateways and the respective
organisations by providing correct data.

EVENT:

In the event folder open terminal and do following commands
-npm init
-npm install fabric-network@2.2.4
Add event.js,profile.js,blockEventListener.js,contractEventListener.js,tnxEventListener.js
-Run the following commands in order
-node blockEventListener.js
-node contractEventListener.js
-node txnEventListener.js

API:

In the api folder open terminal and do following commands
-npm i express
-npm init
-npm g nodemon
Add client.js,profile.js,event.js
Create app.js and add the following functions
-Then run the command—- node app.js
-And open the postman and copy the url as given app.js folder
-And perform operations of get, post,put and delete
-For put and post give data in the body and set raw to json.